def CheckUpdate(msg): #54
	try:
		uversao = urllib2.urlopen( "https://raw.githubusercontent.com/fenixflix/repository.fenix.flix/master/plugin.video.fenix.flix/version.txt" ).read().replace('','').replace('\r','')
		uversao = re.compile('[a-zA-Z\.\d]+').findall(uversao)[0]
		#xbmcgui.Dialog().ok(Versao, uversao)
		if uversao != Versao:
			Update()
			#xbmc.executebuiltin("XBMC.Container.Refresh()")
		elif msg==True:
			xbmcgui.Dialog().ok('[B][COLOR firebrick]FENIX[/COLOR] [COLOR white]FLIX[/COLOR][/B]', "O addon já esta com a última versão: [COLOR firebrick]4.6.9[/COLOR] de "+Versao+"\nAs atualizações normalmente são automáticas, caso não atualize, baixe o zip atualizado em:\n[COLOR firebrick]www.fenixflix.ml[/COLOR]\nUse esse recurso caso não atualize!")
			xbmc.executebuiltin("XBMC.Container.Refresh()")
	except urllib2.URLError, e:
		if msg==True:
			xbmcgui.Dialog().ok('[B][COLOR firebrick]FENIX[/COLOR] [COLOR white]FLIX[/COLOR][/B]', "Não foi possível checar")


def Update():
	Path = xbmc.translatePath( xbmcaddon.Addon().getAddonInfo('path') ).decode("utf-8")
	try:
		fonte = urllib2.urlopen( "https://raw.githubusercontent.com/fenixflix/repository.fenix.flix/master/plugin.video.fenix.flix/default.py" ).read().replace('','')
		prog = re.compile('checkintegrity').findall(fonte)
		if prog:
			py = os.path.join( Path, "default.py")
			file = open(py, "w")
			file.write(fonte)
			file.close()
		fonte = urllib2.urlopen( "https://raw.githubusercontent.com/fenixflix/repository.fenix.flix/master/plugin.video.fenix.flix/resources/settings.xml" ).read().replace('','')
		prog = re.compile('</settings>').findall(fonte)
		if prog:
			py = os.path.join( Path, "resources/settings.xml")
			file = open(py, "w")
			file.write(fonte)
			file.close()
		fonte = urllib2.urlopen( "https://raw.githubusercontent.com/fenixflix/repository.fenix.flix/master/plugin.video.fenix.flix/addon.xml" ).read().replace('','')
		prog = re.compile('</addon>').findall(fonte)
		if prog:
			py = os.path.join( Path, "addon.xml")
			file = open(py, "w")
			file.write(fonte)
			file.close()
		xbmc.executebuiltin("Notification({0}, {1}, 9000, {2})".format(_addonname_, "Atualizando o addon. Aguarde...", icon))
		xbmc.sleep(0)
	except:
		xbmcgui.Dialog().ok('[B][COLOR firebrick]FENIX[/COLOR] [COLOR white]FLIX[/COLOR][/B]', "OBS: Add-on desatualizaldo! Não se preocupe estamos atualizando o addon. Aguarde um momento! Caso ocorrer erro, por favor Atualize o Add-on no Site Oficial: [COLOR firebrick]www.fenixflix.ml[/COLOR]")


def FENIX_SKindex():
    import xbmc
    import webbrowser
    dialog = xbmcgui.Dialog()
    link = dialog.select('[B][COLOR white]SUPORTE[/COLOR] [COLOR firebrick]FÊNIX[/COLOR] [COLOR white]FLIX[COLOR white] OFICIAL[/COLOR][/B]', ['[COLOR firebrick]Fênix[/COLOR][COLOR white] Flix[/COLOR][COLOR white] -[/COLOR] SITE OFICIAL', '[COLOR firebrick]Fênix[/COLOR] [COLOR white]Flix[/COLOR][COLOR white] -[/COLOR] WHATSAPP','[COLOR firebrick]Fênix[/COLOR] [COLOR white]Flix[/COLOR][COLOR white] -[/COLOR] TELEGRAM','[COLOR firebrick]Fênix[/COLOR] [COLOR white]Flix[/COLOR][COLOR white] -[/COLOR] TWITTER','[COLOR firebrick]Fênix[/COLOR] [COLOR white]Flix[/COLOR][COLOR white] -[/COLOR] IPTV P2P','[COLOR firebrick]Fênix[/COLOR] [COLOR white]Flix[/COLOR][COLOR white] -[/COLOR] DOAÇÃO','ENTRAR NO [COLOR firebrick]FÊNIX[/COLOR] [COLOR white]FLIX[/COLOR]'])
      
    if link == 0:
     if xbmc . getCondVisibility ( 'system.platform.android' ) :
         xbmc . executebuiltin ( 'StartAndroidActivity(,android.intent.action.VIEW,,%s)' % ( 'https://fenixflix.ml' ) )
     else:
        ost = webbrowser . open ( 'https://fenixflix.ml' )
  
    if link == 1:
     if xbmc . getCondVisibility ( 'system.platform.android' ) :
         ost = xbmc . executebuiltin ( 'StartAndroidActivity(,android.intent.action.VIEW,,%s)' % ( 'https://bit.ly/whatsapp-fenixflix' ) )
     else:
        webbrowser . open ( 'https://bit.ly/whatsapp-fenixflix' ) 

    if link == 2:
     if xbmc . getCondVisibility ( 'system.platform.android' ) :
         xbmc . executebuiltin ( 'StartAndroidActivity(,android.intent.action.VIEW,,%s)' % ( 'https://t.me/addonfenixflix' ) )
     else:
        ost = webbrowser . open ( 'https://t.me/addonfenixflix' )
    
    if link == 3:
     if xbmc . getCondVisibility ( 'system.platform.android' ) :
         xbmc . executebuiltin ( 'StartAndroidActivity(,android.intent.action.VIEW,,%s)' % ( 'http://twitter.com/addonfenixflix' ) )
     else:
        ost = webbrowser . open ( 'http://twitter.com/addonfenixflix' )
 
    if link == 4:
     if xbmc . getCondVisibility ( 'system.platform.android' ) :
         ost = xbmc . executebuiltin ( 'StartAndroidActivity(,android.intent.action.VIEW,,%s)' % ( 'http://bit.ly/app-iptv-p2p' ) )
     else:
        ost = webbrowser . open ( 'http://bit.ly/app-iptv-p2p' )

    if link == 5:
     if xbmc . getCondVisibility ( 'system.platform.android' ) :
         ost = xbmc . executebuiltin ( 'StartAndroidActivity(,android.intent.action.VIEW,,%s)' % ( 'https://t.me/addonfenixflix/5176' ) )
     else:
        ost = webbrowser . open ( 'https://t.me/addonfenixflix/5176' )